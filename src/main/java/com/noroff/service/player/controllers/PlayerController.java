package com.noroff.service.player.controllers;

import com.noroff.service.player.data_access.*;
import com.noroff.service.player.models.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/player")
public class PlayerController {

    PlayerRepository playerRepository = new PlayerRepository();

    @GetMapping("/{username}")
    public ResponseEntity<Player> getPlayer(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        Player player = playerRepository.getPlayer(username);

        if(player == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(player, status);
    }

    UserRepository userRepository = new UserRepository();
    PersonRepository personRepository = new PersonRepository();
    OptionalRepository optionalRepository = new OptionalRepository();
    RoleRepository roleRepository = new RoleRepository();
    TeamRepository teamRepository = new TeamRepository();
    SchoolRepository schoolRepository = new SchoolRepository();
    MatchRepository matchRepository = new MatchRepository();
    ChildParentRepository childParentRepository = new ChildParentRepository();
    MessageRepository messageRepository = new MessageRepository();

    @GetMapping("/{username}/full")
    public ResponseEntity<PlayerInformation> getPersonalInformation(@PathVariable String username) {
        HttpStatus status = HttpStatus.OK;
        User user = userRepository.getSpecificUser(username);
        Person person = personRepository.getPersonById(user.getPerson_id());
        Optional optionalInformation = optionalRepository.getSpecificOptionalInformation(username);
        List<Child_parent> parents = childParentRepository.getParents(username);
        Role role = roleRepository.getRole(user.getRole_id());
        Player player = playerRepository.getPlayer(username);
        Team team = teamRepository.getTeam(player.getTeam_id());
        School school = schoolRepository.getSchool(team.getSchool_id());
        List<MatchDetailed> matches = matchRepository.getMatchesWhereIncluded(username);
        List<Message> messages = messageRepository.getReceivedMessages(username);

        if(person == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(new PlayerInformation(person, optionalInformation, parents, user, role, team, player, school, matches, messages), status);
    }
}
