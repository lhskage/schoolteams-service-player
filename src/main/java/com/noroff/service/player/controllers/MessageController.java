package com.noroff.service.player.controllers;

import com.noroff.service.player.data_access.MessageRepository;
import com.noroff.service.player.models.Message;
import com.noroff.service.player.models.Pair;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;


@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/message")
public class MessageController {
    MessageRepository messageRepository = new MessageRepository();

    @GetMapping("/sent/{username}")
    public ResponseEntity<ArrayList<Message>> getSentMessages(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Message> messages = messageRepository.getSentMessages(username);

        if(messages == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(messages, status);
    }

    @GetMapping("/inbox/{username}")
    public ResponseEntity<ArrayList<Message>> getReceivedMessages(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Message> messages = messageRepository.getReceivedMessages(username);

        if(messages == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(messages, status);
    }

    @PostMapping("/send")
    public ResponseEntity<Message> sendMessage(@RequestBody Message message){
        Pair<HttpStatus, Integer> status = messageRepository.sendMessage(message);

        if(status.getU() != -1){
            message.setMessage_id(status.getU());
        }

        if(status.getT() != HttpStatus.CREATED){
            return new ResponseEntity<>(null, status.getT());
        }

        return new ResponseEntity<>(message, status.getT());
    }
}