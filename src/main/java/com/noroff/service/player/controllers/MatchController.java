package com.noroff.service.player.controllers;

import com.noroff.service.player.data_access.MatchRepository;
import com.noroff.service.player.models.MatchDetailed;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/matches")
public class MatchController {
    MatchRepository matchRepository = new MatchRepository();

    @GetMapping()
    public ResponseEntity<ArrayList<MatchDetailed>> getAllMatches(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> matches = matchRepository.getAllMatches();

        if(matches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(matches, status);
    }

    @GetMapping("/upcoming")
    public ResponseEntity<ArrayList<MatchDetailed>> getUpcomingMatches(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> upcomingMatches = matchRepository.getUpcomingMatches();

        if(upcomingMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(upcomingMatches, status);
    }

    @GetMapping("/previous")
    public ResponseEntity<ArrayList<MatchDetailed>> getPreviousMatches(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> previousMatches = matchRepository.getPreviousMatches();

        if(previousMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(previousMatches, status);
    }

    @GetMapping("/cancelled")
    public ResponseEntity<ArrayList<MatchDetailed>> getCancelledMatches(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> cancelledMatches = matchRepository.getCancelledMatches();

        if(cancelledMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(cancelledMatches, status);
    }

    @GetMapping("/{username}")
    public ResponseEntity<ArrayList<MatchDetailed>> getMatchesWhereIncluded(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> includedMatches = matchRepository.getMatchesWhereIncluded(username);

        if(includedMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(includedMatches, status);
    }

    @GetMapping("/upcoming/{username}")
    public ResponseEntity<ArrayList<MatchDetailed>> getUpcomingMatchesByPlayer(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> upcomingMatches = matchRepository.getUpcomingMatchesByPlayer(username);

        if(upcomingMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(upcomingMatches, status);
    }



    @GetMapping("/previous/{username}")
    public ResponseEntity<ArrayList<MatchDetailed>> getPreviousMatchesByPlayer(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> previousMatches = matchRepository.getPreviousMatchesByPlayer(username);

        if(previousMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(previousMatches, status);
    }

    @GetMapping("/school/{school_id}")
    public ResponseEntity<ArrayList<MatchDetailed>> getMatchesBySchool(@PathVariable int school_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> matches = matchRepository.getMatchesBySchool(school_id);

        if(matches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(matches, status);
    }

    @GetMapping("/upcoming/school/{school_id}")
    public ResponseEntity<ArrayList<MatchDetailed>> getUpcomingMatchesBySchool(@PathVariable int school_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> upcomingMatches = matchRepository.getUpcomingMatchesBySchool(school_id);

        if(upcomingMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(upcomingMatches, status);
    }

    @GetMapping("/previous/school/{school_id}")
    public ResponseEntity<ArrayList<MatchDetailed>> getPreviousMatchesBySchool(@PathVariable int school_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> previousMatches = matchRepository.getPreviousMatchesBySchool(school_id);

        if(previousMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(previousMatches, status);
    }

    @GetMapping("/cancelled/school/{school_id}")
    public ResponseEntity<ArrayList<MatchDetailed>> getCancelledMatchesBySchool(@PathVariable int school_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> cancelledMatches = matchRepository.getCancelledMatchesBySchool(school_id);

        if(cancelledMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(cancelledMatches, status);
    }

    @GetMapping("/team/{team_id}")
    public ResponseEntity<ArrayList<MatchDetailed>> getMatchesByTeam(@PathVariable int team_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> matches = matchRepository.getMatchesByTeam(team_id);

        if(matches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(matches, status);
    }

    @GetMapping("/upcoming/team/{team_id}")
    public ResponseEntity<ArrayList<MatchDetailed>> getUpcomingMatchesByTeam(@PathVariable int team_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> upcomingMatches = matchRepository.getUpcomingMatchesByTeam(team_id);

        if(upcomingMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(upcomingMatches, status);
    }

    @GetMapping("/previous/team/{team_id}")
    public ResponseEntity<ArrayList<MatchDetailed>> getPreviousMatchesByTeam(@PathVariable int team_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> previousMatches = matchRepository.getPreviousMatchesByTeam(team_id);

        if(previousMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(previousMatches, status);
    }

    @GetMapping("/cancelled/team/{team_id}")
    public ResponseEntity<ArrayList<MatchDetailed>> getCancelledMatchesByTeam(@PathVariable int team_id){
        HttpStatus status = HttpStatus.OK;
        ArrayList<MatchDetailed> cancelledMatches = matchRepository.getCancelledMatchesByTeam(team_id);

        if(cancelledMatches == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(cancelledMatches, status);
    }
}