package com.noroff.service.player.controllers;

import com.noroff.service.player.data_access.ChildParentRepository;
import com.noroff.service.player.models.Child_parent;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/parents")
public class ChildParentController {
    ChildParentRepository childParentRepository = new ChildParentRepository();

    @GetMapping("/{child}")
    public ResponseEntity<ArrayList<Child_parent>> getParents(@PathVariable String child){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Child_parent> parents = childParentRepository.getParents(child);

        if(parents == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(parents, status);
    }
}
