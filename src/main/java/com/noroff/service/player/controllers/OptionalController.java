package com.noroff.service.player.controllers;

import com.noroff.service.player.data_access.OptionalRepository;
import com.noroff.service.player.models.Optional;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/optional")
public class OptionalController {

    OptionalRepository optionalRepository = new OptionalRepository();

    @GetMapping("/{optional_id}")
    public ResponseEntity<Optional> getOptionalInformationById(@PathVariable int optional_id){
        HttpStatus status = HttpStatus.OK;
        Optional optional = optionalRepository.getOptionalInformationById(optional_id);

        if(optional == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(optional, status);
    }

    @GetMapping("/user/{username}")
    public ResponseEntity<Optional> getSpecificOptionalInformation(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        Optional optional = optionalRepository.getSpecificOptionalInformation(username);

        if(optional == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(optional, status);
    }

    @GetMapping("/shared/{username}")
    public ResponseEntity<Optional> getSharedPlayerInformation(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        Optional optional = optionalRepository.getSharedPlayerInformation(username);

        if(optional == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(optional, status);
    }

    @PutMapping("/shared/{username}")
    public ResponseEntity<Optional> changeSharedPlayerInformation(@PathVariable String username, @RequestBody Optional changes){
        HttpStatus status = optionalRepository.changeSharedPlayerInformation(username, changes);

        if(changes == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(changes, status);
    }
}
