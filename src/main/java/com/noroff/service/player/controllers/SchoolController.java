package com.noroff.service.player.controllers;

import com.noroff.service.player.data_access.SchoolRepository;
import com.noroff.service.player.models.School;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/school")
public class SchoolController {
    SchoolRepository schoolRepository = new SchoolRepository();

    @GetMapping("/player/{username}")
    public ResponseEntity<School> getSchoolByPlayer(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        School stats = schoolRepository.getSchoolByPlayer(username);

        if(stats == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(stats, status);
    }

    @GetMapping("/{school_id}")
    public ResponseEntity<School> getSchool(@PathVariable int school_id){
        HttpStatus status = HttpStatus.OK;
        School school = schoolRepository.getSchool(school_id);

        if(school == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(school, status);
    }
}
