package com.noroff.service.player.controllers;

import com.noroff.service.player.data_access.RoleRepository;
import com.noroff.service.player.models.Role;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/role")
public class RoleController {
    RoleRepository roleRepository = new RoleRepository();

    @GetMapping("/{person_id}")
    public ResponseEntity<Role> getRole(@PathVariable int person_id){
        HttpStatus status = HttpStatus.OK;
        Role role = roleRepository.getRole(person_id);

        if(role == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(role, status);
    }
}
