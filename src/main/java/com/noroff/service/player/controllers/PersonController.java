package com.noroff.service.player.controllers;

import com.noroff.service.player.data_access.PersonRepository;
import com.noroff.service.player.models.Person;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/person")
public class PersonController {
    PersonRepository personRepository = new PersonRepository();

    @GetMapping("/username/{username}")
    public ResponseEntity<Person> getUserByUsername(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        Person person = personRepository.getPersonByUsername(username);

        if(person == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(person, status);
    }

    @GetMapping("/id/{person_id}")
    public ResponseEntity<Person> getUser(@PathVariable int person_id){
        HttpStatus status = HttpStatus.OK;
        Person person = personRepository.getPersonById(person_id);

        if(person == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(person, status);
    }
}
