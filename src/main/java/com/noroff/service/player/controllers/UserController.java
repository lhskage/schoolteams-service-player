package com.noroff.service.player.controllers;

import com.noroff.service.player.data_access.UserRepository;
import com.noroff.service.player.models.Toggle2FA;
import com.noroff.service.player.models.TwoFaQr;
import com.noroff.service.player.models.User;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/user")
public class UserController {

    UserRepository userRepository = new UserRepository();

    @GetMapping("/{username}")
    public ResponseEntity<User> getSpecificUser(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        User user = userRepository.getSpecificUser(username);

        if(user == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(user, status);
    }

    @GetMapping("/admin")
    public ResponseEntity<ArrayList<User>> getAllAdministrators(){
        HttpStatus status = HttpStatus.OK;
        ArrayList<User> users = userRepository.getAllAdministrators();

        if(users == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(users, status);
    }

    @PatchMapping("/2fa")
    public ResponseEntity<Boolean> toggleActive2fa(@RequestBody Toggle2FA toggle2FA){
        HttpStatus status = HttpStatus.OK;
        Boolean active = userRepository.update2FA(toggle2FA);

        if(active == null){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        check2faData(toggle2FA.getUsername());

        return new ResponseEntity<>(active, status);
    }

    @GetMapping("/2fa/active/{username}")
    public ResponseEntity<Boolean> get2faActive(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        Boolean active = userRepository.getActive(username);

        if(active == null){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(active, status);
    }

    @GetMapping("/2fa/qr/{username}")
    public ResponseEntity<TwoFaQr> get2faQrUrl(@PathVariable String username){
        HttpStatus status = HttpStatus.OK;
        String url = userRepository.getQrUrl(username);

        if(url == null){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }

        return new ResponseEntity<>(new TwoFaQr(url), status);
    }

    private void check2faData(String username){
        TwoFaQr url = get2faQrUrl(username).getBody();
        if(url == null){
            //Sends request to verify the token
            String generateUrl = "https://schoolteams-service-auth.herokuapp.com/api/v1/2fa/generate";
            RestTemplate restTemplate = new RestTemplate();

            Map<String, String> requestBody = new HashMap();
            requestBody.put("username", username);

            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);

            HttpEntity<?> requestEntity = new HttpEntity<>(requestBody, headers);

            ResponseEntity<?> responseEntity = restTemplate.postForEntity(
                    generateUrl,
                    requestEntity,
                    String.class);
            HttpStatus statusCode = responseEntity.getStatusCode();
            //If the response code is 200, then the token is valid
            boolean authorized = statusCode == HttpStatus.OK;
        }
    }
}