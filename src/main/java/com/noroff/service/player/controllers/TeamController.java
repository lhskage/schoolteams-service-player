package com.noroff.service.player.controllers;

import com.noroff.service.player.data_access.TeamRepository;
import com.noroff.service.player.models.Team;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@CrossOrigin("*")
@RequestMapping("/api/v1/team")
public class TeamController {

    TeamRepository teamRepository = new TeamRepository();

    @GetMapping("/{team_id}")
    public ResponseEntity<Team> getTeam(@PathVariable int team_id){
        HttpStatus status = HttpStatus.OK;
        Team team = teamRepository.getTeam(team_id);

        if(team == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(team, status);
    }

    @GetMapping("/player/{player}")
    public ResponseEntity<ArrayList<Team>> getTeamByPlayer(@PathVariable String player){
        HttpStatus status = HttpStatus.OK;
        ArrayList<Team> person = teamRepository.getTeamByPlayer(player);

        if(person == null){
            status = HttpStatus.NOT_FOUND;
            return new ResponseEntity<>(null, status);
        }

        return new ResponseEntity<>(person, status);
    }
}
