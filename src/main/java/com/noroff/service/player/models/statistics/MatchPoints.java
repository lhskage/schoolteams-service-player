package com.noroff.service.player.models.statistics;

public class MatchPoints {
    private int id;
    private int match_points;

    public MatchPoints(int id, int match_points) {
        this.id = id;
        this.match_points = match_points;
    }

    public MatchPoints() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getMatch_points() {
        return match_points;
    }

    public void setMatch_points(int match_points) {
        this.match_points = match_points;
    }
}
