package com.noroff.service.player.models;

public class Person {
    private int id;
    private String firstname;
    private String lastname;
    private String email;
    private String gender;
    private int optional_id;

    // Constructor
    public Person(int id, String firstname, String lastname, String email, String gender, int optional_id) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.gender = gender;
        this.optional_id = optional_id;
    }

    public Person(String firstname, String lastname, String email, String gender, int optional_id) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.email = email;
        this.gender = gender;
        this.optional_id = optional_id;
    }

    public Person() {
    }

    // Getters and setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getOptional_id() {
        return optional_id;
    }

    public void setOptional_id(int optional_id) {
        this.optional_id = optional_id;
    }
}
