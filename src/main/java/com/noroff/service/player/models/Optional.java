package com.noroff.service.player.models;

import java.time.LocalDateTime;

public class Optional {
    private int id;
    private LocalDateTime date_of_birth;
    private String mobile_number;
    private String profile_picture;
    private String medical_notes;
    private boolean date_of_birth_shared;
    private boolean mobile_number_shared;
    private boolean profile_picture_shared;
    private boolean medical_notes_shared;

    // Constructor
    public Optional(int id, LocalDateTime date_of_birth, String mobile_number, String profile_picture, String medical_notes) {
        this.id = id;
        this.date_of_birth = date_of_birth;
        this.mobile_number = mobile_number;
        this.profile_picture = profile_picture;
        this.medical_notes = medical_notes;
    }

    public Optional(LocalDateTime date_of_birth, String mobile_number, String profile_picture, String medical_notes) {
        this.date_of_birth = date_of_birth;
        this.mobile_number = mobile_number;
        this.profile_picture = profile_picture;
        this.medical_notes = medical_notes;
    }

    public Optional(LocalDateTime date_of_birth, String mobile_number, String profile_picture, String medical_notes, boolean date_of_birth_shared, boolean mobile_number_shared, boolean profile_picture_shared, boolean medical_notes_shared) {
        this.date_of_birth = date_of_birth;
        this.mobile_number = mobile_number;
        this.profile_picture = profile_picture;
        this.medical_notes = medical_notes;
        this.date_of_birth_shared = date_of_birth_shared;
        this.mobile_number_shared = mobile_number_shared;
        this.profile_picture_shared = profile_picture_shared;
        this.medical_notes_shared = medical_notes_shared;
    }

    public Optional(int id, LocalDateTime date_of_birth, String mobile_number, String profile_picture, String medical_notes, boolean date_of_birth_shared, boolean mobile_number_shared, boolean profile_picture_shared, boolean medical_notes_shared) {
        this.id = id;
        this.date_of_birth = date_of_birth;
        this.mobile_number = mobile_number;
        this.profile_picture = profile_picture;
        this.medical_notes = medical_notes;
        this.date_of_birth_shared = date_of_birth_shared;
        this.mobile_number_shared = mobile_number_shared;
        this.profile_picture_shared = profile_picture_shared;
        this.medical_notes_shared = medical_notes_shared;
    }

    public Optional() {
    }

    // Getters and setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(LocalDateTime date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getMedical_notes() {
        return medical_notes;
    }

    public void setMedical_notes(String medical_notes) {
        this.medical_notes = medical_notes;
    }

    public boolean isDate_of_birth_shared() {
        return date_of_birth_shared;
    }

    public void setDate_of_birth_shared(boolean date_of_birth_shared) {
        this.date_of_birth_shared = date_of_birth_shared;
    }

    public boolean isMobile_number_shared() {
        return mobile_number_shared;
    }

    public void setMobile_number_shared(boolean mobile_number_shared) {
        this.mobile_number_shared = mobile_number_shared;
    }

    public boolean isProfile_picture_shared() {
        return profile_picture_shared;
    }

    public void setProfile_picture_shared(boolean profile_picture_shared) {
        this.profile_picture_shared = profile_picture_shared;
    }

    public boolean isMedical_notes_shared() {
        return medical_notes_shared;
    }

    public void setMedical_notes_shared(boolean medical_notes_shared) {
        this.medical_notes_shared = medical_notes_shared;
    }
}
