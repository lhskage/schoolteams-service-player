package com.noroff.service.player.models;

import java.time.LocalDateTime;
import java.util.List;

public class PlayerInformation {
    private String firstname;
    private String lastname;
    private String email;
    private String gender;
    private List<Child_parent> parents;
    private String username;
    private String role;
    private String team;
    private String number;
    private String school;
    private LocalDateTime date_of_birth;
    private String mobile_number;
    private String profile_picture;
    private String medical_notes;
    private List<MatchDetailed> matches;
    private List<Message> messages;

    // Constructors
    public PlayerInformation(Person person, Optional optional, List<Child_parent> parents, User user, Role role, Team team,
                             Player player, School school, List<MatchDetailed> matches, List<Message> messages) {
        this.firstname = person.getFirstname();
        this.lastname = person.getLastname();
        this.email = person.getEmail();
        this.gender = person.getGender();
        this.parents = parents;
        this.username = user.getUsername();
        this.role = role.getName();
        this.team = team.getName();
        this.number = player.getPlayer_number();
        this.school = school.getName();
        this.date_of_birth = optional.getDate_of_birth();
        this.mobile_number = optional.getMobile_number();
        this.profile_picture = optional.getProfile_picture();
        this.medical_notes = optional.getMedical_notes();
        this.matches = matches;
        this.messages = messages;
    }
    // Getters and setters
    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public String getTeam() {
        return team;
    }

    public void setTeam(String team) {
        this.team = team;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public LocalDateTime getDate_of_birth() {
        return date_of_birth;
    }

    public void setDate_of_birth(LocalDateTime date_of_birth) {
        this.date_of_birth = date_of_birth;
    }

    public String getMobile_number() {
        return mobile_number;
    }

    public void setMobile_number(String mobile_number) {
        this.mobile_number = mobile_number;
    }

    public String getProfile_picture() {
        return profile_picture;
    }

    public void setProfile_picture(String profile_picture) {
        this.profile_picture = profile_picture;
    }

    public String getMedical_notes() {
        return medical_notes;
    }

    public void setMedical_notes(String medical_notes) {
        this.medical_notes = medical_notes;
    }

    public List<Child_parent> getParents() {
        return parents;
    }

    public void setParents(List<Child_parent> parents) {
        this.parents = parents;
    }

    public List<MatchDetailed> getMatches() {
        return matches;
    }

    public void setMatches(List<MatchDetailed> matches) {
        this.matches = matches;
    }


}
