package com.noroff.service.player.models;

public class PostalCode {
    private int post_code;
    private String post_city;

    // Constructor
    public PostalCode(int post_code, String post_city) {
        this.post_code = post_code;
        this.post_city = post_city;
    }

    // Getters and setters
    public int getPost_code() {
        return post_code;
    }

    public void setPost_code(int post_code) {
        this.post_code = post_code;
    }

    public String getPost_city() {
        return post_city;
    }

    public void setPost_city(String post_city) {
        this.post_city = post_city;
    }
}
