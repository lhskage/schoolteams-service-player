package com.noroff.service.player.models;

import java.time.LocalDateTime;

public class Match {
    private int id;
    private LocalDateTime start_time;
    private String result;
    private boolean cancelled;
    private String comment;
    private int location_id;
    private int team_1_id;
    private int team_2_id;

    // Constructor
    public Match(int id, LocalDateTime start_time, String result, boolean cancelled, String comment, int location_id, int team_1_id, int team_2_id) {
        this.id = id;
        this.start_time = start_time;
        this.result = result;
        this.cancelled = cancelled;
        this.comment = comment;
        this.location_id = location_id;
        this.team_1_id = team_1_id;
        this.team_2_id = team_2_id;
    }

    public Match(LocalDateTime start_time, String result, boolean cancelled, String comment, int location_id, int team_1_id, int team_2_id) {
        this.start_time = start_time;
        this.result = result;
        this.cancelled = cancelled;
        this.comment = comment;
        this.location_id = location_id;
        this.team_1_id = team_1_id;
        this.team_2_id = team_2_id;
    }

    public Match() {
    }

    // Getters and setter
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public LocalDateTime getStart_time() {
        return start_time;
    }

    public void setStart_time(LocalDateTime start_time) {
        this.start_time = start_time;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }

    public int getTeam_1_id() {
        return team_1_id;
    }

    public void setTeam_1_id(int team_1_id) {
        this.team_1_id = team_1_id;
    }

    public int getTeam_2_id() {
        return team_2_id;
    }

    public void setTeam_2_id(int team_2_id) {
        this.team_2_id = team_2_id;
    }
}
