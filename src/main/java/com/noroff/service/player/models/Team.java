package com.noroff.service.player.models;

public class Team {
    private int id;
    private String name;
    private int school_id;
    private String coach;
    private int sport_id;

    // Constructor
    public Team(int id, String name, int school_id, String coach, int sport_id) {
        this.id = id;
        this.name = name;
        this.school_id = school_id;
        this.coach = coach;
        this.sport_id = sport_id;
    }

    public Team(String name, int school_id, String coach, int sport_id) {
        this.name = name;
        this.school_id = school_id;
        this.coach = coach;
        this.sport_id = sport_id;
    }

    public Team() {
    }

    // Getters and setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getSchool_id() {
        return school_id;
    }

    public void setSchool_id(int school_id) {
        this.school_id = school_id;
    }

    public String getCoach() {
        return coach;
    }

    public void setCoach(String coach) {
        this.coach = coach;
    }

    public int getSport_id() {
        return sport_id;
    }

    public void setSport_id(int sport_id) {
        this.sport_id = sport_id;
    }
}
