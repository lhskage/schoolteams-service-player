package com.noroff.service.player.models;

public class Player {
    private String username;
    private int team_id;
    private String player_number;

    // Constructor
    public Player(String username, int team_id, String player_number) {
        this.username = username;
        this.team_id = team_id;
        this.player_number = player_number;
    }

    public Player() {
    }

    // Getters and setters
    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getTeam_id() {
        return team_id;
    }

    public void setTeam_id(int team_id) {
        this.team_id = team_id;
    }

    public String getPlayer_number() {
        return player_number;
    }

    public void setPlayer_number(String player_number) {
        this.player_number = player_number;
    }
}
