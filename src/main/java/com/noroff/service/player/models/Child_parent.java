package com.noroff.service.player.models;

public class Child_parent {
    public String parent_1;
    public String child;
    public String parent_2;

    // Constructor
    public Child_parent(String parent_1, String child, String parent_2) {
        this.parent_1 = parent_1;
        this.child = child;
        this.parent_2 = parent_2;
    }

    public Child_parent(String parent_1, String parent_2) {
        this.parent_1 = parent_1;
        this.parent_2 = parent_2;
    }



    // Getters and setters
    public String getParent_1() {
        return parent_1;
    }

    public void setParent_1(String parent_1) {
        this.parent_1 = parent_1;
    }

    public String getChild() {
        return child;
    }

    public void setChild(String child) {
        this.child = child;
    }

    public String getParent_2() {
        return parent_2;
    }

    public void setParent_2(String parent_2) {
        this.parent_2 = parent_2;
    }
}
