package com.noroff.service.player.data_access;

import com.noroff.service.player.models.MatchDetailed;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class MatchRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    // Get matches
    public ArrayList<MatchDetailed> getAllMatches() {
        ArrayList<MatchDetailed> matches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner," +
                            "start_time, location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id");
            ResultSet result = prep.executeQuery();

            while(result.next()){matches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return matches;
    }

    public ArrayList<MatchDetailed> getUpcomingMatches() {
        ArrayList<MatchDetailed> upcomingMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner," +
                            "start_time, location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "WHERE match.start_time >= NOW() " +
                            "AND match.cancelled = false");
            ResultSet result = prep.executeQuery();

            while(result.next()){upcomingMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return upcomingMatches;
    }

    public ArrayList<MatchDetailed> getPreviousMatches() {
        ArrayList<MatchDetailed> cancelledMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "WHERE match.start_time <= NOW() " +
                            "AND match.cancelled = false");
            ResultSet result = prep.executeQuery();

            while(result.next()){cancelledMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return cancelledMatches;
    }

    public ArrayList<MatchDetailed> getCancelledMatches() {
        ArrayList<MatchDetailed> cancelledMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "WHERE cancelled = TRUE");
            ResultSet result = prep.executeQuery();

            while(result.next()){cancelledMatches.add(new MatchDetailed(
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return cancelledMatches;
    }

    // Get matches by player (username)
    public ArrayList<MatchDetailed> getUpcomingMatchesByPlayer(String username) {
        ArrayList<MatchDetailed> upcomingMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT DISTINCT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN player AS home " +
                            "on match.team_1_id = home.team_id " +
                            "JOIN player AS away " +
                            "on match.team_2_id = away.team_id " +
                            "WHERE (home.user_username = ? " +
                            "OR away.user_username = ?) AND " +
                            "match.start_time >= NOW() " +
                            "AND match.cancelled = false");
            prep.setString(1, username);
            prep.setString(2, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){upcomingMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return upcomingMatches;
    }

    public ArrayList<MatchDetailed> getPreviousMatchesByPlayer(String username) {
        ArrayList<MatchDetailed> previousMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT DISTINCT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN player AS home " +
                            "on match.team_1_id = home.team_id " +
                            "JOIN player AS away " +
                            "on match.team_2_id = away.team_id " +
                            "WHERE (home.user_username = ? " +
                            "OR away.user_username = ?) AND " +
                            "match.start_time <= NOW() " +
                            "AND match.cancelled = false");
            prep.setString(1, username);
            prep.setString(2, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){previousMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return previousMatches;
    }

    public ArrayList<MatchDetailed> getMatchesWhereIncluded(String username) {

        ArrayList<MatchDetailed> includedMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT DISTINCT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN player AS home " +
                            "on match.team_1_id = home.team_id " +
                            "JOIN player AS away " +
                            "on match.team_2_id = away.team_id " +
                            "WHERE home.user_username = ? " +
                            "OR away.user_username = ?;");
            prep.setString(1, username);
            prep.setString(2, username);

            ResultSet result = prep.executeQuery();

            while(result.next()){includedMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return includedMatches;
    }

    // Get matches by school (school_id)
    public ArrayList<MatchDetailed> getMatchesBySchool(int school_id) {
        ArrayList<MatchDetailed> matches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, " +
                            "location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN school " +
                            "ON team.school_id = school.id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id " +
                            "JOIN school as home_team_school " +
                            "ON home_team.school_id = home_team_school.id " +
                            "JOIN school AS away_team_school " +
                            "ON away_team.school_id = away_team_school.id " +
                            "WHERE home_team_school.id = ?" +
                            "OR away_team_school.id = ?");

            prep.setInt(1, school_id);
            prep.setInt(2, school_id);

            ResultSet result = prep.executeQuery();

            while(result.next()){matches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return matches;
    }

    public ArrayList<MatchDetailed> getUpcomingMatchesBySchool(int school_id) {
        ArrayList<MatchDetailed> upcomingMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, " +
                            "location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN school " +
                            "ON team.school_id = school.id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id " +
                            "JOIN school as home_team_school " +
                            "ON home_team.school_id = home_team_school.id " +
                            "JOIN school AS away_team_school " +
                            "ON away_team.school_id = away_team_school.id " +
                            "WHERE (home_team_school.id = ?" +
                            "OR away_team_school.id = ?) AND " +
                            "match.start_time >= NOW() AND match.cancelled = false");

            prep.setInt(1, school_id);
            prep.setInt(2, school_id);

            ResultSet result = prep.executeQuery();

            while(result.next()){upcomingMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return upcomingMatches;
    }

    public ArrayList<MatchDetailed> getPreviousMatchesBySchool(int school_id) {
        ArrayList<MatchDetailed> previousMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, " +
                            "location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN school " +
                            "ON team.school_id = school.id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id " +
                            "JOIN school as home_team_school " +
                            "ON home_team.school_id = home_team_school.id " +
                            "JOIN school AS away_team_school " +
                            "ON away_team.school_id = away_team_school.id " +
                            "WHERE (home_team_school.id = ?" +
                            "OR away_team_school.id = ?) AND " +
                            "match.start_time <= NOW() AND match.cancelled = false");
            prep.setInt(1, school_id);
            prep.setInt(2, school_id);

            ResultSet result = prep.executeQuery();

            while(result.next()){previousMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return previousMatches;
    }

    public ArrayList<MatchDetailed> getCancelledMatchesBySchool(int school_id) {
        ArrayList<MatchDetailed> cancelledMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, " +
                            "location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN school " +
                            "ON team.school_id = school.id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id " +
                            "JOIN school as home_team_school " +
                            "ON home_team.school_id = home_team_school.id " +
                            "JOIN school AS away_team_school " +
                            "ON away_team.school_id = away_team_school.id " +
                            "WHERE (home_team_school.id = ?" +
                            "OR away_team_school.id = ?) AND match.cancelled = true");
            prep.setInt(1, school_id);
            prep.setInt(2, school_id);

            ResultSet result = prep.executeQuery();

            while(result.next()){cancelledMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return cancelledMatches;
    }

    // Get matches by team (team_id)
    public ArrayList<MatchDetailed> getMatchesByTeam(int team_id) {
        ArrayList<MatchDetailed> matches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, " +
                            "location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN school " +
                            "ON team.school_id = school.id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id  " +
                            "WHERE (home_team.id = ? " +
                            "OR away_team.id = ?)");
            prep.setInt(1, team_id);
            prep.setInt(2, team_id);

            ResultSet result = prep.executeQuery();

            while(result.next()){matches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return matches;
    }

    public ArrayList<MatchDetailed> getUpcomingMatchesByTeam(int team_id) {
        ArrayList<MatchDetailed> upcomingMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, " +
                            "location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN school " +
                            "ON team.school_id = school.id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id  " +
                            "WHERE (home_team.id = ? " +
                            "OR away_team.id = ?) AND " +
                            "match.start_time >= NOW() AND match.cancelled = false");
            prep.setInt(1, team_id);
            prep.setInt(2, team_id);

            ResultSet result = prep.executeQuery();

            while(result.next()){upcomingMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return upcomingMatches;
    }

    public ArrayList<MatchDetailed> getPreviousMatchesByTeam(int team_id) {
        ArrayList<MatchDetailed> previousMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, " +
                            "location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN school " +
                            "ON team.school_id = school.id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id  " +
                            "WHERE (home_team.id = ? " +
                            "OR away_team.id = ?) AND " +
                            "match.start_time <= NOW() AND match.cancelled = false");
            prep.setInt(1, team_id);
            prep.setInt(2, team_id);

            ResultSet result = prep.executeQuery();

            while(result.next()){previousMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return previousMatches;
    }

    public ArrayList<MatchDetailed> getCancelledMatchesByTeam(int team_id) {
        ArrayList<MatchDetailed> cancelledMatches = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT match.id, (SELECT name FROM team WHERE id = match.team_1_id) AS home_team, " +
                            "(SELECT name FROM team WHERE id = match.team_2_id) AS away_team, " +
                            "(SELECT home_team_goals FROM match_result WHERE match_id = match.id) AS home_team_goals, " +
                            "(SELECT away_team_goals FROM match_result WHERE match_id = match.id) AS away_team_goals, " +
                            "(SELECT winner FROM match_result WHERE match_id = match.id) AS winner, start_time, " +
                            "location.street, location.number, location.name, match.comment FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN school " +
                            "ON team.school_id = school.id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id  " +
                            "WHERE (home_team.id = ? " +
                            "OR away_team.id = ?) AND " +
                            "match.cancelled = true");
            prep.setInt(1, team_id);
            prep.setInt(2, team_id);

            ResultSet result = prep.executeQuery();

            while(result.next()){cancelledMatches.add(new MatchDetailed(
                    result.getInt("id"),
                    result.getString("home_team"),
                    result.getString("away_team"),
                    result.getInt("home_team_goals"),
                    result.getInt("away_team_goals"),
                    result.getString("winner"),
                    result.getObject("start_time", java.time.LocalDateTime.class),
                    result.getString("street"),
                    result.getString("number"),
                    result.getString("name"),
                    result.getString("comment")
            ));
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return cancelledMatches;
    }

}
