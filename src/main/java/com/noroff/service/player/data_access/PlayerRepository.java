package com.noroff.service.player.data_access;

import com.noroff.service.player.models.Player;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class PlayerRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public Player getPlayer(String username) {
        Player player = null;

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM player WHERE user_username = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){player = new Player(
                    result.getString("user_username"),
                    result.getInt("team_id"),
                    result.getString("player_number")
            );}

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return player;
    }
}
