package com.noroff.service.player.data_access;

import com.noroff.service.player.models.Optional;
import org.springframework.http.HttpStatus;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class OptionalRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public Optional getOptionalInformationById(int optional_id){
        Optional optionalInformation = null;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM optional " +
                    "WHERE optional.id = ?");
            prep.setInt(1, optional_id);
            ResultSet result = prep.executeQuery();

            while(result.next()){optionalInformation = new Optional(
                    result.getInt("id"),
                    result.getObject("date_of_birth", java.time.LocalDateTime.class),
                    result.getString("mobile_number"),
                    result.getString("profile_picture"),
                    result.getString("medical_notes"),
                    result.getBoolean("dob_shared"),
                    result.getBoolean("mobile_number_shared"),
                    result.getBoolean("profile_picture_shared"),
                    result.getBoolean("medical_notes_shared")
            );}

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return optionalInformation;
    }

    public Optional getSpecificOptionalInformation(String username){
        Optional optionalInformation = null;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM optional " +
                    "JOIN person " +
                    "ON optional.id = person.optional_id " +
                    "JOIN users " +
                    "ON person.id = users.person_id " +
                    "WHERE username = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){optionalInformation = new Optional(
                    result.getInt("id"),
                    result.getObject("date_of_birth", java.time.LocalDateTime.class),
                    result.getString("mobile_number"),
                    result.getString("profile_picture"),
                    result.getString("medical_notes"),
                    result.getBoolean("dob_shared"),
                    result.getBoolean("mobile_number_shared"),
                    result.getBoolean("profile_picture_shared"),
                    result.getBoolean("medical_notes_shared")
            );}

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return optionalInformation;
    }

    public Optional getSharedPlayerInformation(String username) {
        Optional optionalPlayerInformation = null;

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "select case when dob_shared='true' " +
                            "   then date_of_birth " +
                            "   else null end as date_of_birth, " +
                            "case when mobile_number_shared='true' " +
                            "   then mobile_number " +
                            "   else null end as mobile_number, " +
                            "case when profile_picture_shared='true' " +
                            "   then profile_picture " +
                            "   else null end as profile_picture, " +
                            "case when medical_notes_shared='true' " +
                            "   then medical_notes " +
                            "   else null end as medical_notes ," +
                            "optional.dob_shared, " +
                            "optional.mobile_number_shared, " +
                            "optional.profile_picture_shared, " +
                            "optional.medical_notes_shared " +
                            "from optional " +
                            "JOIN person " +
                            "ON optional.id = person.optional_id " +
                            "JOIN users " +
                            "ON person.id = users.person_id " +
                            "JOIN player " +
                            "ON users.username = player.user_username " +
                            "WHERE users.username = ?;");

            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){optionalPlayerInformation = new Optional(
                    result.getObject("date_of_birth", java.time.LocalDateTime.class),
                    result.getString("mobile_number"),
                    result.getString("profile_picture"),
                    result.getString("medical_notes"),
                    result.getBoolean("dob_shared"),
                    result.getBoolean("mobile_number_shared"),
                    result.getBoolean("profile_picture_shared"),
                    result.getBoolean("medical_notes_shared")
            );}

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return optionalPlayerInformation;
    }

    public HttpStatus changeSharedPlayerInformation(String username, Optional optional) {
        HttpStatus success;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("UPDATE optional " +
                            "SET dob_shared = ?, " +
                            "mobile_number_shared = ?, " +
                            "profile_picture_shared = ?, " +
                            "medical_notes_shared = ? " +
                            "WHERE optional.id = (SELECT optional.id " +
                            "FROM optional " +
                            "JOIN person " +
                            "ON optional.id = person.optional_id " +
                            "JOIN users " +
                            "ON person.id = users.person_id " +
                            "WHERE users.username = ?);");

            prep.setBoolean(1, optional.isDate_of_birth_shared());
            prep.setBoolean(2, optional.isMobile_number_shared());
            prep.setBoolean(3, optional.isProfile_picture_shared());
            prep.setBoolean(4, optional.isMedical_notes_shared());
            prep.setString(5, username);

            int result = prep.executeUpdate();
            if(result != 0){
                success = HttpStatus.ACCEPTED;
            }else{
                success = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return HttpStatus.INTERNAL_SERVER_ERROR;
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }
}
