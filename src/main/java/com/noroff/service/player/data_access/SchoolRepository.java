package com.noroff.service.player.data_access;

import com.noroff.service.player.models.School;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class SchoolRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public School getSchoolByPlayer(String username) {
        School school = new School();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT school.id, school.name FROM school " +
                            "JOIN team " +
                            "ON school.id = team.school_id " +
                            "JOIN player " +
                            "ON team.id = player.team_id " +
                            "WHERE player.user_username = ?");
            prep.setString(1, username);

            ResultSet result = prep.executeQuery();
            while(result.next()) {
                school = new School(
                        result.getInt("id"),
                        result.getString("name")
                );
            }
        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return school;
    }

    public School getSchool(int school_id){
        School school = null;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM school WHERE id = ?");
            prep.setInt(1, school_id);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                school = new School(
                    result.getInt("id"),
                    result.getString("name")
                );
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return school;
    }
}
