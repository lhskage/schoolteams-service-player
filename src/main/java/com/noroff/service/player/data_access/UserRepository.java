package com.noroff.service.player.data_access;

import com.noroff.service.player.models.Toggle2FA;
import com.noroff.service.player.models.User;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class UserRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public User getSpecificUser(String username){
        User user = null;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM users WHERE username = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                user = new User(
                        result.getString("username"),
                        result.getString("password"),
                        result.getInt("person_id"),
                        result.getInt("role_id")
                );
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return user;
    }

    public ArrayList<User> getAllAdministrators() {
        ArrayList<User> users = new ArrayList<>();
        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM users WHERE role_id = 1");
            ResultSet result = prep.executeQuery();


            while(result.next()){ users.add(new User(
                    result.getString("username"),
                    result.getInt("person_id"),
                    result.getInt("role_id")
            ));
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return users;
    }

    public Boolean getActive(String username){
        Boolean active = null;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT \"2fa_active\" FROM users WHERE username = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                active = result.getBoolean("2fa_active");
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return active;
    }

    public Boolean update2FA(Toggle2FA toggle2FA){
        Boolean success = null;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("UPDATE public.users SET \"2fa_active\"=?" +
                            " WHERE username = ?;");
            prep.setBoolean(1, toggle2FA.getActive());
            prep.setString(2, toggle2FA.getUsername());

            int result = prep.executeUpdate();
            if(result != 0){
                success = toggle2FA.getActive();
            }else{
                success = null;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return false;
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return success;
    }

    public String getQrUrl(String username){
        String url = null;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT tfa_qr_url FROM users WHERE username = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                url = result.getString("tfa_qr_url");
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return url;
    }

}