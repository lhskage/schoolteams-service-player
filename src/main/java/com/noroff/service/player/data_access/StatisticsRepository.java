package com.noroff.service.player.data_access;

import com.noroff.service.player.models.statistics.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class StatisticsRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    // Statistics
    public MatchesPlayed getNumberOfPlayedMatchesBySchool(int school_id) {
        MatchesPlayed matchesPlayed = new MatchesPlayed();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT COUNT(*) AS played FROM (SELECT match.id, match.start_time, home_team.name, away_team.name, " +
                            "match_result.home_team_goals, match_result.away_team_goals, " +
                            "match_result.winner, " +
                            "CASE " +
                            "WHEN match_result.winner = 'H' THEN home_team.id " +
                            "WHEN match_result.winner = 'A' THEN away_team.id " +
                            "ELSE null " +
                            "END AS match_winner " +
                            "FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id " +
                            "JOIN school as home_team_school " +
                            "ON home_team.school_id = home_team_school.id " +
                            "JOIN school AS away_team_school    " +
                            "ON away_team.school_id = away_team_school.id " +
                            "JOIN match_result   " +
                            "ON match.id = match_result.match_id " +
                            "WHERE (home_team.school_id = ? " +
                            "OR away_team.school_id  = ?)" +
                            "AND match.cancelled = false) AS data");
            prep.setInt(1, school_id);
            prep.setInt(2, school_id);

            ResultSet result = prep.executeQuery();
            while(result.next()) {
                matchesPlayed.setId(school_id);
                matchesPlayed.setMatches_played(result.getInt("played"));
            }


        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return matchesPlayed;
    }

    public MatchesWon getNumberOfWinsBySchool(int school_id) {
        MatchesWon matchesWon = new MatchesWon();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT COUNT(*) AS wins FROM (SELECT match.id, match.start_time, home_team.name, away_team.name, " +
                            "match_result.home_team_goals, match_result.away_team_goals, " +
                            "match_result.winner, " +
                            "CASE " +
                            "WHEN match_result.winner = 'H' THEN home_team.id " +
                            "WHEN match_result.winner = 'A' THEN away_team.id " +
                            "ELSE null " +
                            "END AS match_winner " +
                            "FROM team  " +
                            "JOIN match  " +
                            "ON match.team_1_id = team.id  " +
                            "JOIN location  " +
                            "ON match.location_id = location.id  " +
                            "JOIN team as home_team  " +
                            "ON match.team_1_id = home_team.id  " +
                            "JOIN team as away_team  " +
                            "ON match.team_2_id = away_team.id " +
                            "JOIN school as home_team_school  " +
                            "ON home_team.school_id = home_team_school.id  " +
                            "JOIN school AS away_team_school  " +
                            "ON away_team.school_id = away_team_school.id " +
                            "JOIN match_result " +
                            "ON match.id = match_result.match_id " +
                            "WHERE (home_team.school_id = ? OR away_team.school_id  = ?)) " +
                            "AS data " +
                            "WHERE data.match_winner IN (SELECT team.id FROM team WHERE school_id = ?)");
            prep.setInt(1, school_id);
            prep.setInt(2, school_id);
            prep.setInt(3, school_id);

            ResultSet result = prep.executeQuery();
            while(result.next()) {
                matchesWon = new MatchesWon(
                        school_id,
                        result.getInt("wins")
                );
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return matchesWon;
    }

    public MatchesLost getNumberOfLossesBySchool(int school_id) {
        MatchesLost matchesLost = new MatchesLost();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT COUNT(*) AS losses FROM (SELECT match.id, match.start_time, home_team.name, away_team.name, " +
                            "match_result.home_team_goals, match_result.away_team_goals, " +
                            "match_result.winner, " +
                            "CASE " +
                            "WHEN match_result.winner = 'H' THEN home_team.id " +
                            "WHEN match_result.winner = 'A' THEN away_team.id " +
                            "ELSE null " +
                            "END AS match_winner " +
                            "FROM team  " +
                            "JOIN match  " +
                            "ON match.team_1_id = team.id  " +
                            "JOIN location  " +
                            "ON match.location_id = location.id  " +
                            "JOIN team as home_team  " +
                            "ON match.team_1_id = home_team.id  " +
                            "JOIN team as away_team  " +
                            "ON match.team_2_id = away_team.id " +
                            "JOIN school as home_team_school  " +
                            "ON home_team.school_id = home_team_school.id  " +
                            "JOIN school AS away_team_school  " +
                            "ON away_team.school_id = away_team_school.id " +
                            "JOIN match_result " +
                            "ON match.id = match_result.match_id " +
                            "WHERE (home_team.school_id = ? OR away_team.school_id  = ?)) " +
                            "AS data " +
                            "WHERE data.match_winner NOT IN (SELECT team.id FROM team WHERE school_id = ?)");
            prep.setInt(1, school_id);
            prep.setInt(2, school_id);
            prep.setInt(3, school_id);

            ResultSet result = prep.executeQuery();
            while(result.next()) {
                matchesLost = new MatchesLost(
                        school_id,
                        result.getInt("losses")
                );
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return matchesLost;
    }

    public MatchesDrawn getNumberOfDrawsBySchool(int school_id) {
        MatchesDrawn matchesDrawn = new MatchesDrawn();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT COUNT(*) AS draws FROM (SELECT match.id, match.start_time, home_team.name, away_team.name, " +
                            "match_result.home_team_goals, match_result.away_team_goals, " +
                            "match_result.winner, " +
                            "CASE " +
                            "WHEN match_result.winner = 'H' THEN home_team.id " +
                            "WHEN match_result.winner = 'A' THEN away_team.id " +
                            "ELSE null " +
                            "END AS match_winner " +
                            "FROM team  " +
                            "JOIN match  " +
                            "ON match.team_1_id = team.id  " +
                            "JOIN location  " +
                            "ON match.location_id = location.id  " +
                            "JOIN team as home_team  " +
                            "ON match.team_1_id = home_team.id  " +
                            "JOIN team as away_team  " +
                            "ON match.team_2_id = away_team.id " +
                            "JOIN school as home_team_school  " +
                            "ON home_team.school_id = home_team_school.id  " +
                            "JOIN school AS away_team_school  " +
                            "ON away_team.school_id = away_team_school.id " +
                            "JOIN match_result " +
                            "ON match.id = match_result.match_id " +
                            "WHERE (home_team.school_id = ? OR away_team.school_id  = ?)) " +
                            "AS data " +
                            "WHERE data.match_winner IS null");
            prep.setInt(1, school_id);
            prep.setInt(2, school_id);

            ResultSet result = prep.executeQuery();
            while(result.next()) {
                matchesDrawn = new MatchesDrawn(
                        school_id,
                        result.getInt("draws")
                );
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return matchesDrawn;
    }

    public Statistics getStatsBySchool(int school_id) {
        int matches_played = getNumberOfPlayedMatchesBySchool(school_id).getMatches_played();
        int wins = getNumberOfWinsBySchool(school_id).getMatches_won();
        int losses = getNumberOfLossesBySchool(school_id).getMatches_lost();
        int draws = getNumberOfDrawsBySchool(school_id).getMatches_drawn();
        int points = (wins*3) + (draws*1);

        Statistics stats = new Statistics(school_id, matches_played, wins, draws, losses, points);

        return stats;
    }

    public MatchesPlayed getNumberOfPlayedMatchesByTeam(int team_id) {
        MatchesPlayed matchesPlayed = new MatchesPlayed();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT COUNT(*) AS played FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN school " +
                            "ON team.school_id = school.id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id " +
                            "JOIN school as home_team_school " +
                            "ON home_team.school_id = home_team_school.id " +
                            "JOIN school AS away_team_school " +
                            "ON away_team.school_id = away_team_school.id " +
                            "WHERE (home_team.id = ? " +
                            "OR away_team.id = ?) " +
                            "AND match.start_time < NOW() " +
                            "AND match.cancelled = false");
            prep.setInt(1, team_id);
            prep.setInt(2, team_id);

            ResultSet result = prep.executeQuery();
            while(result.next()) {
                matchesPlayed.setId(team_id);
                matchesPlayed.setMatches_played(result.getInt("played"));
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return matchesPlayed;
    }

    public MatchesWon getNumberOfWinsByTeam(int team_id) {
        MatchesWon stats = new MatchesWon();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT COUNT(*) AS wins FROM (SELECT match.id, match.start_time, home_team.name, away_team.name, " +
                            "match_result.home_team_goals, match_result.away_team_goals, " +
                            "match_result.winner, " +
                            "CASE " +
                            "WHEN match_result.winner = 'H' THEN home_team.id " +
                            "WHEN match_result.winner = 'A' THEN away_team.id " +
                            "ELSE null " +
                            "END AS match_winner " +
                            "FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN school " +
                            "ON team.school_id = school.id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id " +
                            "JOIN match_result " +
                            "ON match.id = match_result.match_id " +
                            "AND match.cancelled = false) AS query " +
                            "WHERE query.match_winner = ?");
            prep.setInt(1, team_id);

            ResultSet result = prep.executeQuery();
            while(result.next()) {
                stats = new MatchesWon(
                        team_id,
                        result.getInt("wins")
                );
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return stats;
    }

    public MatchesLost getNumberOfLossesByTeam(int team_id) {
        MatchesLost matchesLost = new MatchesLost();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT COUNT(*) AS losses FROM (SELECT match.id, match.start_time, home_team.name, away_team.name, " +
                            "match_result.home_team_goals, match_result.away_team_goals, " +
                            "match_result.winner, " +
                            "CASE " +
                            "WHEN match_result.winner = 'H' THEN home_team.id " +
                            "WHEN match_result.winner = 'A' THEN away_team.id " +
                            "ELSE null " +
                            "END AS match_winner " +
                            "FROM team  " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id " +
                            "JOIN school " +
                            "ON team.school_id = school.id " +
                            "JOIN team as home_team " +
                            "ON match.team_1_id = home_team.id " +
                            "JOIN team as away_team " +
                            "ON match.team_2_id = away_team.id " +
                            "JOIN match_result " +
                            "ON match.id = match_result.match_id " +
                            "WHERE home_team.id = ? OR " +
                            "away_team.id = ? " +
                            "AND match.cancelled = false) AS query " +
                            "WHERE query.match_winner != ?");
            prep.setInt(1, team_id);
            prep.setInt(2, team_id);
            prep.setInt(3, team_id);

            ResultSet result = prep.executeQuery();
            while(result.next()) {
                matchesLost = new MatchesLost(
                        team_id,
                        result.getInt("losses")
                );
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return matchesLost;
    }

    public MatchesDrawn getNumberOfDrawsByTeam(int team_id) {
        MatchesDrawn matchesDrawn = new MatchesDrawn();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT COUNT(*) AS draws FROM (SELECT match.id, match.start_time, home_team.name, away_team.name,  " +
                            "match_result.home_team_goals, match_result.away_team_goals,  " +
                            "match_result.winner,  " +
                            "CASE  " +
                            "WHEN match_result.winner = 'H' THEN home_team.id  " +
                            "WHEN match_result.winner = 'A' THEN away_team.id  " +
                            "ELSE null  " +
                            "END AS match_winner  " +
                            "FROM team   " +
                            "JOIN match   " +
                            "ON match.team_1_id = team.id   " +
                            "JOIN location   " +
                            "ON match.location_id = location.id   " +
                            "JOIN school  " +
                            "ON team.school_id = school.id   " +
                            "JOIN team as home_team   " +
                            "ON match.team_1_id = home_team.id   " +
                            "JOIN team as away_team   " +
                            "ON match.team_2_id = away_team.id   " +
                            "JOIN match_result  " +
                            "ON match.id = match_result.match_id  " +
                            "WHERE (home_team.id = ? OR away_team.id = ?)  " +
                            "AND match_result.winner = 'D'" +
                            "AND match.cancelled = false) AS query");
            prep.setInt(1, team_id);
            prep.setInt(2, team_id);

            ResultSet result = prep.executeQuery();
            while(result.next()) {
                matchesDrawn = new MatchesDrawn(
                        team_id,
                        result.getInt("draws")
                );
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return matchesDrawn;
    }

    public Statistics getStatsByTeam(int team_id) {
        int matches_played = getNumberOfPlayedMatchesByTeam(team_id).getMatches_played();
        int wins = getNumberOfWinsByTeam(team_id).getMatches_won();
        int losses = getNumberOfLossesByTeam(team_id).getMatches_lost();
        int draws = getNumberOfDrawsByTeam(team_id).getMatches_drawn();
        int points = (wins*3) + (draws*1);

        Statistics stats = new Statistics(team_id, matches_played, wins, draws, losses, points);

        return stats;
    }

    public MatchesPlayed getNumberOfPlayedMatchesByPlayer(String username) {
        MatchesPlayed matchesPlayed = new MatchesPlayed();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT COUNT(*) AS played FROM (SELECT DISTINCT match.id, match.start_time, home_team.name, away_team.name, " +
                            "match_result.home_team_goals, match_result.away_team_goals, " +
                            "match_result.winner " +
                            "FROM team " +
                            "JOIN match " +
                            "ON match.team_1_id = team.id " +
                            "JOIN location " +
                            "ON match.location_id = location.id  " +
                            "JOIN team as home_team  " +
                            "ON match.team_1_id = home_team.id  " +
                            "JOIN team as away_team  " +
                            "ON match.team_2_id = away_team.id  " +
                            "JOIN school as home_team_school  " +
                            "ON home_team.school_id = home_team_school.id  " +
                            "JOIN school AS away_team_school  " +
                            "ON away_team.school_id = away_team_school.id " +
                            "JOIN match_result    " +
                            "ON match.id = match_result.match_id  " +
                            "JOIN player AS home_team_player " +
                            "ON home_team.id = home_team_player.team_id " +
                            "JOIN player AS away_team_player " +
                            "ON away_team.id = away_team_player.team_id " +
                            "WHERE match.start_time < NOW() AND " +
                            "home_team_player.user_username = ?  OR away_team_player.user_username = ? " +
                            "AND match.cancelled = false) AS data");
            prep.setString(1, username);
            prep.setString(2, username);

            ResultSet result = prep.executeQuery();
            while(result.next()) {
                matchesPlayed.setMatches_played(result.getInt("played"));
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return matchesPlayed;
    }

    public MatchesWon getNumberOfWinsByPlayer(String username) {
        MatchesWon stats = new MatchesWon();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT COUNT(*) AS wins FROM (SELECT DISTINCT match.id, match.start_time, home_team.name, away_team.name,  " +
                            "match_result.home_team_goals, match_result.away_team_goals,  " +
                            "match_result.winner, " +
                            "CASE  " +
                            "WHEN match_result.winner = 'H' THEN home_team.id  " +
                            "WHEN match_result.winner = 'A' THEN away_team.id  " +
                            "ELSE null  " +
                            "END AS match_winner  " +
                            "FROM team  " +
                            "JOIN match  " +
                            "ON match.team_1_id = team.id  " +
                            "JOIN location  " +
                            "ON match.location_id = location.id  " +
                            "JOIN team as home_team  " +
                            "ON match.team_1_id = home_team.id  " +
                            "JOIN team as away_team  " +
                            "ON match.team_2_id = away_team.id  " +
                            "JOIN school as home_team_school  " +
                            "ON home_team.school_id = home_team_school.id  " +
                            "JOIN school AS away_team_school  " +
                            "ON away_team.school_id = away_team_school.id " +
                            "JOIN match_result    " +
                            "ON match.id = match_result.match_id  " +
                            "JOIN player AS home_team_player " +
                            "ON home_team.id = home_team_player.team_id " +
                            "JOIN player AS away_team_player " +
                            "ON away_team.id = away_team_player.team_id " +
                            "WHERE match.cancelled = false " +
                            "AND match.start_time < NOW()) AS data " +
                            "JOIN player " +
                            "ON data.match_winner = player.team_id " +
                            "AND player.user_username = ?");
            prep.setString(1, username);

            ResultSet result = prep.executeQuery();
            while(result.next()) {
                stats = new MatchesWon(
                        result.getInt("wins")
                );
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return stats;
    }

    public MatchesDrawn getNumberOfDrawsByPlayer(String username) {
        MatchesDrawn stats = new MatchesDrawn();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT COUNT(*) AS draws FROM (SELECT DISTINCT match.id, match.start_time, home_team.name, away_team.name,  " +
                            "match_result.home_team_goals, match_result.away_team_goals,  " +
                            "match_result.winner, " +
                            "CASE  " +
                            "WHEN match_result.winner = 'H' THEN home_team.id  " +
                            "WHEN match_result.winner = 'A' THEN away_team.id  " +
                            "ELSE null  " +
                            "END AS match_winner  " +
                            "FROM team  " +
                            "JOIN match  " +
                            "ON match.team_1_id = team.id  " +
                            "JOIN location  " +
                            "ON match.location_id = location.id  " +
                            "JOIN team as home_team  " +
                            "ON match.team_1_id = home_team.id  " +
                            "JOIN team as away_team  " +
                            "ON match.team_2_id = away_team.id  " +
                            "JOIN school as home_team_school  " +
                            "ON home_team.school_id = home_team_school.id  " +
                            "JOIN school AS away_team_school  " +
                            "ON away_team.school_id = away_team_school.id " +
                            "JOIN match_result    " +
                            "ON match.id = match_result.match_id  " +
                            "JOIN player AS home_team_player " +
                            "ON home_team.id = home_team_player.team_id " +
                            "JOIN player AS away_team_player " +
                            "ON away_team.id = away_team_player.team_id " +
                            "WHERE match.cancelled = false " +
                            "AND match.team_1_id IN (SELECT team_id FROM player WHERE user_username = ?) " +
                            "OR match.team_2_id IN (SELECT team_id FROM player WHERE user_username = ?) " +
                            "AND match.start_time < NOW()) AS query " +
                            "WHERE query.match_winner IS NULL");
            prep.setString(1, username);
            prep.setString(2, username);

            ResultSet result = prep.executeQuery();
            while(result.next()) {
                stats = new MatchesDrawn(
                        result.getInt("draws")
                );
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return stats;
    }

    public MatchesLost getNumberOfLossesByPlayer(String username) {
        MatchesLost stats = new MatchesLost();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT COUNT(*) AS losses FROM (SELECT DISTINCT match.id, match.start_time, home_team.name, away_team.name,    " +
                            "match_result.home_team_goals, match_result.away_team_goals,     " +
                            "match_result.winner,     " +
                            "CASE    " +
                            "WHEN match_result.winner = 'H' THEN home_team.id    " +
                            "WHEN match_result.winner = 'A' THEN away_team.id    " +
                            "ELSE null    " +
                            "END AS match_winner    " +
                            "FROM team     " +
                            "JOIN match     " +
                            "ON match.team_1_id = team.id     " +
                            "JOIN location     " +
                            "ON match.location_id = location.id     " +
                            "JOIN school     " +
                            "ON team.school_id = school.id     " +
                            "JOIN team as home_team    " +
                            "ON match.team_1_id = home_team.id    " +
                            "JOIN team as away_team     " +
                            "ON match.team_2_id = away_team.id    " +
                            "JOIN match_result     " +
                            "ON match.id = match_result.match_id    " +
                            "AND match.cancelled = false) AS query     " +
                            "WHERE query.match_winner IS NOT NULL AND query.match_winner NOT IN (SELECT team_id FROM player WHERE user_username = ?)");
            prep.setString(1, username);

            ResultSet result = prep.executeQuery();
            while(result.next()) {
                stats = new MatchesLost(
                        result.getInt("losses")
                );
            }

        } catch(Exception exception){
            System.out.println(exception.toString());
        } finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return stats;
    }

    public PlayerStatistic getStatsByPlayer(String username) {
        int matches_played = getNumberOfPlayedMatchesByPlayer(username).getMatches_played();
        int wins = getNumberOfWinsByPlayer(username).getMatches_won();
        int draws = getNumberOfDrawsByPlayer(username).getMatches_drawn();
        int losses = getNumberOfLossesByPlayer(username).getMatches_lost();
        int points = (wins*3) + (draws*1);

        PlayerStatistic stats = new PlayerStatistic(username, matches_played, wins, draws, losses, points);

        return stats;
    }
}