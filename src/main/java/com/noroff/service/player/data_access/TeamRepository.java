package com.noroff.service.player.data_access;

import com.noroff.service.player.models.Team;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class TeamRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public Team getTeam(int team_id){
        Team team = null;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM team WHERE id = ?");
            prep.setInt(1, team_id);
            ResultSet result = prep.executeQuery();

            while(result.next()){
                team = new Team(
                    result.getInt("id"),
                        result.getString("name"),
                        result.getInt("school_id"),
                        result.getString("coach"),
                        result.getInt("sport_id")
                );
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return null;
        }finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return team;
    }

    public ArrayList<Team> getTeamByPlayer(String player) {
        ArrayList<Team> teams = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT team.id, team.name, team.school_id, team.coach, team.sport_id " +
                    "FROM team " +
                    "JOIN player " +
                    "ON team.id = player.team_id " +
                    "WHERE player.user_username = ?;");
            prep.setString(1, player);
            ResultSet result = prep.executeQuery();

            while(result.next()) {
                teams.add(
                    new Team(
                            result.getInt("id"),
                            result.getString("name"),
                            result.getInt("school_id"),
                            result.getString("coach"),
                            result.getInt("sport_id")
                    )
                );
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return teams;
    }
}
