package com.noroff.service.player.data_access;

import com.noroff.service.player.models.Child_parent;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

public class ChildParentRepository {
    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public ArrayList<Child_parent> getParents(String child) {
        ArrayList<Child_parent> parents = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement(
                    "SELECT * FROM child_parent WHERE child = ?");
            prep.setString(1, child);

            ResultSet result = prep.executeQuery();

            while(result.next()){parents.add(new Child_parent(
                result.getString("parent_1"),
                result.getString("child"),
                result.getString("parent_2")
                ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return parents;
    }
}
