package com.noroff.service.player.data_access;

import com.noroff.service.player.models.Message;
import com.noroff.service.player.models.Pair;
import org.springframework.http.HttpStatus;

import java.sql.*;
import java.util.ArrayList;

public class MessageRepository {

    private String URL = System.getenv("JDBC_CONN_STRING");
    private Connection connection = null;

    public ArrayList<Message> getSentMessages(String username) {
        ArrayList<Message> messages = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM message WHERE sender = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){messages.add(new Message(
                    result.getString("sender"),
                    result.getString("receiver"),
                    result.getString("message"),
                    result.getInt("message_id")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return messages;
    }

    public ArrayList<Message> getReceivedMessages(String username) {
        ArrayList<Message> messages = new ArrayList<>();

        try{
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep = connection.prepareStatement("SELECT * FROM message WHERE receiver = ?");
            prep.setString(1, username);
            ResultSet result = prep.executeQuery();

            while(result.next()){messages.add(new Message(
                    result.getString("sender"),
                    result.getString("receiver"),
                    result.getString("message"),
                    result.getInt("message_id")
            ));
            }

        }catch(Exception exception){
            System.out.println(exception.toString());
        }
        finally {
            try{
                connection.close();
            } catch (Exception exception){
                System.out.println(exception.toString());
            }
        }
        return messages;
    }

    public Pair<HttpStatus, Integer> sendMessage(Message message) {
        HttpStatus status;
        int last_inserted_id;

        try {
            connection = DriverManager.getConnection(URL);
            PreparedStatement prep =
                    connection.prepareStatement("INSERT INTO message(sender, receiver, message) " +
                            " VALUES (?, ?, ?);", Statement.RETURN_GENERATED_KEYS);
            prep.setString(1, message.getSender());
            prep.setString(2, message.getReceiver());
            prep.setString(3, message.getMessage());

            int result = prep.executeUpdate();
            if(result != 0){
                status = HttpStatus.CREATED;
                ResultSet rs = prep.getGeneratedKeys();
                if(rs.next())
                {
                    last_inserted_id = rs.getInt("message_id");
                    return new Pair<>(status, last_inserted_id);
                }
            }else{
                status = HttpStatus.NOT_ACCEPTABLE;
            }

        } catch (Exception e){
            System.out.println(e.toString());
            return new Pair<>(HttpStatus.INTERNAL_SERVER_ERROR, -1);
        } finally {
            try {
                connection.close();
            } catch (Exception e){
                System.out.println(e.toString());
            }
        }

        return new Pair<>(status, -1);
    }
}
